# Engrenages System

![Foundry v10](https://img.shields.io/badge/foundry-v10-green)

This system is using v10 Foundry VTT software (as a MJ you need a unexpensive licence, as a PJ it's free for you, pal, and all your friends). 

Engrenages is a French TTRPG based on Matthieu Gaborit's novels. The last version was written by a team managed by Alexandre Clavel. 
The game is going for English in the hands of Open Sesame Games. 

Alexandre Clavel is working on a SRD based on his system for Engrenages, called Cogs (Engrenages).

Even if the author is aware of this module existence, it's only unofficial for now.

## Usage

manifest link for install : [https://gitlab.com/init-status/fvtt_systems/engrenages/-/raw/main/system.json](https://gitlab.com/init-status/fvtt_systems/engrenages/-/raw/main/system.json)  
zip archive of this FVTT system : [https://gitlab.com/api/v4/projects/44316500/repository/archive.zip](https://gitlab.com/api/v4/projects/44316500/repository/archive.zip)  

## Screenshots

![Beginners guide](screenshots/fvtt-engrenages-journal.png)  
![Archetype](screenshots/fvtt-engrenages-archetype.png)  
![Cephals](screenshots/fvtt-engrenages-cephalie.png)  
![Contacts](screenshots/fvtt-engrenages-contact.png)  
![Roller](screenshots/fvtt-engrenages-roller.png)  


## Thanks and inspirations 

- French Discord Server talking about Foundry VTT
- Illustrations and icons created by Pretre (Discord)
- Carter for this City of Mist sharing and his advices
- Bastien Durel for his Dune work  (GNU LICENSE)