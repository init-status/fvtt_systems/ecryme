/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class EngrenagesItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["engrenages", "sheet", "item"],
      width: 520,
      height: 480,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /** @override */
  get template() {
    const path = "systems/engrenages/templates/item";

    return `${path}/item-${this.item.type}-sheet.html`;
  }

  /* -------------------------------------------- */
  /*
   * Organize and classify Owned Items for Character sheets
   * @private
   */
  async _prepareItems(data) {    
    data.item.img = data.item.img || "systems/engrenages/icons/competence.webp";
    console.log(data);
  }  

  /** @override */
  async getData() {
    // Retrieve base data structure.
    const context = super.getData();
    await this._prepareItems(context);
    // Use a safe clone of the item data for further operations.
    const itemData = context.item;

    // extends data with type custom needs
    switch(this.item.type){
      case 'trait':
          context.traitsvalues = CONFIG.ENGRENAGES.traitsvalues;
          context.traitscategories = CONFIG.ENGRENAGES.traitscategories;
          break;
      case 'weapon':    
          context.zero2six = CONFIG.ENGRENAGES.zero2six;
          break;
      case 'specialization':
          context.skills = CONFIG.ENGRENAGES.skills;
        break;
        case 'contact':
          context.nations = CONFIG.ENGRENAGES.nations;
        break;  
    }
    
    // Retrieve the roll data for TinyMCE editors.
    context.rollData = {};
    let actor = this.object?.parent ?? null;
    if (actor) {
      context.rollData = actor.getRollData();
    }

    // Add the actor's data to context.data for easier access, as well as flags.
    context.system = itemData.system;
    context.flags = itemData.flags;

    return context;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.isEditable) return;

    // Roll handlers, click handlers, etc. would go here.
  }
}
