export const ENGRENAGES = {};

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
/*
 ENGRENAGES.abilities = {
  "str": "ENGRENAGES.AbilityStr",
  "dex": "ENGRENAGES.AbilityDex",
  "con": "ENGRENAGES.AbilityCon",
  "int": "ENGRENAGES.AbilityInt",
  "wis": "ENGRENAGES.AbilityWis",
  "cha": "ENGRENAGES.AbilityCha"
};

ENGRENAGES.abilityAbbreviations = {
  "str": "ENGRENAGES.AbilityStrAbbr",
  "dex": "ENGRENAGES.AbilityDexAbbr",
  "con": "ENGRENAGES.AbilityConAbbr",
  "int": "ENGRENAGES.AbilityIntAbbr",
  "wis": "ENGRENAGES.AbilityWisAbbr",
  "cha": "ENGRENAGES.AbilityChaAbbr"
};

*/
ENGRENAGES.skills = {
  "physical": {
    "label": "ENGRENAGES.SkillType.Physical",
    "data": {
      "ENGRENAGES.AtleticsSkill":"ENGRENAGES.AtleticsSkill",
      "ENGRENAGES.DrivingSkill":"ENGRENAGES.DrivingSkill",
      "ENGRENAGES.FencingSkill":"ENGRENAGES.FencingSkill",
      "ENGRENAGES.BrawlSkill":"ENGRENAGES.BrawlSkill",
      "ENGRENAGES.ShootSkill":"ENGRENAGES.ShootSkill"
    }
  },
  "mental": {
    "label": "ENGRENAGES.SkillType.Mental",
    "data": { 
      "ENGRENAGES.AnthropoMechanicologySkill":"ENGRENAGES.AnthropoMechanicologySkill",
      "ENGRENAGES.EcrymologySkill":"ENGRENAGES.EcrymologySkill",
      "ENGRENAGES.TraumatologySkill":"ENGRENAGES.TraumatologySkill",
      "ENGRENAGES.TraversologySkill":"ENGRENAGES.TraversologySkill",
      "ENGRENAGES.UrbatechnologySkill":"ENGRENAGES.UrbatechnologySkill",
    }
  },
  "social": {
    "label": "ENGRENAGES.SkillType.Social",
    "data": {
      "ENGRENAGES.QuibbleSkill":"ENGRENAGES.QuibbleSkill",
      "ENGRENAGES.CreativitySkill":"ENGRENAGES.CreativitySkill",
      "ENGRENAGES.LoquacitySkill":"ENGRENAGES.LoquacitySkill",
      "ENGRENAGES.SkulduggerySkill":"ENGRENAGES.SkulduggerySkill",
      "ENGRENAGES.PerformanceSkill":"ENGRENAGES.PerformanceSkill"
    }
  }
}

ENGRENAGES.cskills = {
    "label": "ENGRENAGES.CSkills",
    "data": {
      "ENGRENAGES.ElegieSkill":"ENGRENAGES.ElegieSkill",
      "ENGRENAGES.MekaneSkill":"ENGRENAGES.MekaneSkill",
      "ENGRENAGES.EntelechieSkill":"ENGRENAGES.EntelechieSkill",
      "ENGRENAGES.PsycheSkill":"ENGRENAGES.PsycheSkill",
      "ENGRENAGES.ScorieSkill":"ENGRENAGES.ScorieSkill"
    }
}

ENGRENAGES.skillsList = {
  "ENGRENAGES.AtleticsSkill":"ENGRENAGES.AtleticsSkill",
  "ENGRENAGES.DrivingSkill":"ENGRENAGES.DrivingSkill",
  "ENGRENAGES.FencingSkill":"ENGRENAGES.FencingSkill",
  "ENGRENAGES.BrawlSkill":"ENGRENAGES.BrawlSkill",
  "ENGRENAGES.ShootSkill":"ENGRENAGES.ShootSkill",
  "ENGRENAGES.AnthropoMechanicologySkill":"ENGRENAGES.AnthropoMechanicologySkill",
  "ENGRENAGES.EcrymologySkill":"ENGRENAGES.EcrymologySkill",
  "ENGRENAGES.TraumatologySkill":"ENGRENAGES.TraumatologySkill",
  "ENGRENAGES.TraversologySkill":"ENGRENAGES.TraversologySkill",
  "ENGRENAGES.UrbatechnologySkill":"ENGRENAGES.UrbatechnologySkill",
  "ENGRENAGES.QuibbleSkill":"ENGRENAGES.QuibbleSkill",
  "ENGRENAGES.CreativitySkill":"ENGRENAGES.CreativitySkill",
  "ENGRENAGES.LoquacitySkill":"ENGRENAGES.LoquacitySkill",
  "ENGRENAGES.SkulduggerySkill":"ENGRENAGES.SkulduggerySkill",
  "ENGRENAGES.PerformanceSkill":"ENGRENAGES.PerformanceSkill"
}
ENGRENAGES.traitsvalues = {
  '-2':'-2',
  '-1':'-1',
  '-0':'0',
  '1':'+1',
  '2':'+2',
}

ENGRENAGES.zero2six = {
  '0':0,
  '1':1,
  '2':2,
  '3':3,
  '4':4,
  '5':5,
  '6':6
}

ENGRENAGES.zero2ten = {
  '0':0,
  '1':1,
  '2':2,
  '3':3,
  '4':4,
  '5':5,
  '6':6,
  '7':7,
  '8':8,
  '9':9,
  '10':10
}

ENGRENAGES.achievementReroll = {
  '1':7,
  '2':8,
  '3':9,
  '4':10,
  '5':11,
  '6':12
}

ENGRENAGES.conservationReroll = {
  '1':7,
  '2':6,
  '3':5,
  '4':4,
  '5':3,
  '6':2
}


ENGRENAGES.traitscategories = {
  "custom":"ENGRENAGES.CustomTrait",
  "spleen":"ENGRENAGES.SpleenTrait",
  "purpose":"ENGRENAGES.PurposeTrait"
}

ENGRENAGES.nations = {
  "istanie1":{
    "label": "Istanie (îles du couchant)",
    "cities": ["tanger", "argan", "ar'kobah", "ishandra"]
  },
  "istanie2":{
    "label": "Istanie (monts dinariques)",
    "cities": ["montenegro", "ishandra"]
  },
  "istanie3":{
    "label": "Istanie (anatolie)",
    "cities": ["ismyr", "istanbul", "ankara"]
  },
  "pentapolie":{
    "label": "Pentapolie",
    "cities": ["serone", "éole", "relais de l'affrevie", "relais de bragee", "géode", "théorie", "démos", "négoce", "lucé"]
  },
  "venice":{
    "label": "Venice",
    "cities": ["venice"]
  },
  "rhodesiennes":{
    "label": "Provinces rhodesiennes",
    "cities": ["alsyde", "spicule", "urbs", "les syénites"]
  },
  "methalune":{
    "label": "Méthalune",
    "cities": ["méthalune", "ferraille"]
  },
  "gloriana":{
    "label": "Gloriana",
    "cities": ["enclosure", "londres", "camelot", "hivernee"]
  },
  "antipolie":{
    "label": "Antipolie",
    "cities": ["paris", "ithar","candbury","abaya", "relais d'elphiel", "entrelace", "prague", "vienne"]
  },
  "olmune":{
    "label": "Principautés d'Olmune",
    "cities": ["entrepont", "olmune","arssens","braysine"]
  },
  "lansk":{
    "label": "Lansk",
    "cities": ["saint-petersbourg", "hypogée","sancre","moscou", "kiev","kryo"]
  },
  "nordanie":{
    "label": "Nordanie",
    "cities": ["souspente", "gottenborg","solth", "nacre", "dorvik", "mystille"]
  },
  "terraincognita":{
    "label": "Terra Incognita",
    "cities": ["chantier de transécryme"]
  }

}