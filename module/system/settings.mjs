export const registerSettings = function () {
    game.settings.register("engrenages", "game-level", {
        name: game.i18n.localize("ENGRENAGES.WorldSettings.GameLevel.Name"),
        hint: game.i18n.localize("ENGRENAGES.WorldSettings.GameLevel.Hint"),
        scope: "system",
        config: true,
        type: String,
        choices: {
        "e": "Engrenages",
        "c": "Céphale",
        "b": "Bohème",
        "a": "Amertume"
        },
        default: 'e',
        onChange: value => {
        console.log(value);
        }
    });

    game.settings.register("engrenages", "granting_cephalie", {
        name: game.i18n.localize("ENGRENAGES.WorldSettings.GrantingCephales.Label"),
        hint: game.i18n.localize("ENGRENAGES.WorldSettings.GrantingCephales.Description"),
        scope: "system",
        config: true,
        type: Boolean,
        default: !1
    })

}  