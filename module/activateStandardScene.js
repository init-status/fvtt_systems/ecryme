export async function activateStandardScene() {
    let pack, sceneId, scene;
    pack = game.packs.get("beginners-guide");
    sceneId = pack.index.getName("Présentation")._id;
    scene = game.scenes.importFromCompendium(pack, sceneId).then(s => { s.activate() });
}